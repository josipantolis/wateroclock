if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js');
} else {
    console.warn('service workers not supported');
}
