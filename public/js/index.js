// Plant class represents a plant with watering schedule
class Plant {
    constructor(name, schedule, imageUrl) {
        this.name = name;
        this.schedule = schedule;
        this.imageUrl = imageUrl;
        this.id = Date.now();
    }
}

// UI class handles DOM updates
class UI {
    static displayPlants() {
        const plants = Store.getPlantsList();
        plants.forEach(plant => UI.addPlantToList(plant));
    }

    static addPlantToList(plant) {
        const list = document.querySelector('#plants-list');
        const card = document.createElement('div');
        card.className = 'card-panel plant white row';
        card.innerHTML = `
            <img src="${plant.imageUrl}" alt="image of ${plant.name}">
            <div class="plant-details">
                <div class="plant-name">${plant.name}</div>
                <div class="plant-water-schedule">${plant.schedule}</div>
            </div>
            <div class="plant-remove">
                <i id="delete-plant-${plant.id}" class="material-icons">delete_outline</i>
            </div>
        `;
        list.appendChild(card);
    }

    static removePlant(element) {
        if (!element.id.startsWith('delete-plant')) {
            return null;
        }
        const id = element.id.replace('delete-plant-', '');
        element.parentElement.parentElement.remove();
        return id;
    }

    static clearFields() {
        document.querySelector('#name').value = '';
        document.querySelector('#schedule').value = '';
    }

    static readImage(imageFile) {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.onload = () => {
                const img = new Image();
                img.onload = () => {
                    if(img.width === 0 && img.height === 0) {
                        reject(new Error('invalid image'));
                    }
                    resolve(fileReader.result);
                };
                img.src = fileReader.result;
            };
            fileReader.onerror = () => {
                reject(fileReader.error);
            };
            fileReader.readAsDataURL(imageFile);
        });
    }
}

// Store class handles localStorage
class Store {

    static getPlantsMap() {
        const plantsJson = localStorage.getItem('plants');
        const plants = JSON.parse(plantsJson);
        return plants || {};
    }

    static getPlantsList() {
        const plantsObj = Store.getPlantsMap();
        return Object.entries(plantsObj)
            .sort((p1, p2) => p1[0] - p2[0])
            .map(p => p[1]);
    }

    static addPlant(plant) {
        const plants = Store.getPlantsMap();
        plants[plant.id] = plant;
        localStorage.setItem('plants', JSON.stringify(plants));
    }

    static removePlant(id) {
        const plants = Store.getPlantsMap();
        delete plants[id];
        localStorage.setItem('plants', JSON.stringify(plants));
    }

    static numberOfPlants() {
        return Store.getPlantsList().length;
    }
}

// Event Display book
document.addEventListener('DOMContentLoaded', UI.displayPlants);

// Latest plant pic
let PLANT_PIC_FILE;

// Event Add a plant
document.querySelector('#add-form').addEventListener('submit', e => {
    e.stopPropagation();
    e.preventDefault();

    const name = document.querySelector('#name').value;
    const schedule = document.querySelector('#schedule').value;
    UI.readImage(PLANT_PIC_FILE).then(imgData => {
        const plant = new Plant(name, schedule, imgData);
        UI.addPlantToList(plant);
        Store.addPlant(plant);
    
        const sideFormElement = document.querySelector('#side-form')
        const sideNav = M.Sidenav.getInstance(sideFormElement);
        sideNav.close();
        UI.clearFields();
        PLANT_PIC_URL = null;
    });
});

// Event Add plant image
document.querySelector('#input-image').addEventListener('change', e => {
    let file;
    const files = e.target.files;
    for (let i = 0; i < files.length; i++) {
        if (files[i].type.match(/^image\//)) {
            file = files[i];
            break;
        }
    }
    PLANT_PIC_FILE = file;
});

// Event Remove a plant
document.querySelector('#plants-list').addEventListener('click', e => {
    const id = UI.removePlant(e.target);
    if (id) {
        Store.removePlant(id);
    }
});