const siteDynamicCache = 'site-dynamic-v1';
const siteStaticCache = 'site-static-v1';
const siteStaticPaths = [
    '/',
    '/index.html',
    '/js/app.js',
    '/js/materialize.min.js',
    '/js/ui.js',
    '/css/materialize.min.css',
    '/css/styles.css',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://fonts.gstatic.com/s/materialicons/v47/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2',
    '/img/fallback.png',
    '/pages/fallback.html'
];

const limitCacheSize = (name, maxSize) => {
    caches.open(name).then(cache => {
        cache.keys().then(keys => {
            if (keys.length > maxSize) {
                cache.delete(keys[0]).then(limitCacheSize(name, maxSize));
            }
        })
    });
};

self.addEventListener('install', e => {
    e.waitUntil(
        caches.open(siteStaticCache)
            .then(cache => cache.addAll(siteStaticPaths))
    );
})

self.addEventListener('activate', e => {
    e.waitUntil(
        caches.keys().then(keys => Promise.all(keys
            .filter(key => key !== siteStaticCache && key !== siteDynamicCache)
            .map(key => caches.delete(key))
        ))
    );
})

self.addEventListener('fetch', e => {
    e.respondWith(
        caches.match(e.request).then(cacheRes => {
            return cacheRes || fetch(e.request).then(fetchRes => {
                return caches.open(siteDynamicCache).then(cache => {
                    return cache.put(e.request, fetchRes.clone())
                        .then(() => {
                            limitCacheSize(siteDynamicCache, 30);
                            return fetchRes;
                        });
                })
            });
        }).catch(() => {
            const url = e.request.url;
            if (url.endsWith('.html')) {
                return caches.match('/pages/fallback.html');
            }
            if (url.endsWith('.png') || url.endsWith('.jpg')) {
                return caches.match('/img/fallback.png');
            }
        })
    );
})